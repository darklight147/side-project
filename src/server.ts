import App from "./app";

import * as bodyParser from 'body-parser';
import loggerMiddleware from './middlewares/logger';
import HomeController from "./controllers/home.controller";
import helmet from 'helmet';



const app = new App({
    port: 3000,
    controllers: [
        new HomeController()
    ],
    middlewares: [
        bodyParser.json(),
        bodyParser.urlencoded({
            extended: true
        }),
        loggerMiddleware,
        helmet()
    ]
})

app.listen();