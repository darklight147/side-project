import express, { Request, Response, Router} from 'express';

import IControllerBase from '../interfaces/IControllerBase.interface'

class HomeController implements IControllerBase {
    public path = '/';
    public router = Router();

    constructor () {
        this.initRoutes();
    }

    public initRoutes(){
        this.router.get('/', this.index);
    }
    index = (req: Request, res: Response) => {
        res.json({
            status: 200
        })
    }
}

export default HomeController