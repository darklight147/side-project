import {Request, Response, NextFunction} from 'express';

const LoggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
    console.log(`request at ${req.path} | ${req.method}`);
    next();
}

export default LoggerMiddleware;
